import styled from 'vue-styled-components';

const StyledContactContainer = styled.div`
  background: #000;
  padding: 40px 0 60px;
`;

export default StyledContactContainer;

import styled from 'vue-styled-components';

const StyledArtistsContainer = styled.div`
  background: linear-gradient(180deg, #C9009D 0%, #0A0337 100%);
  padding: 40px 0 60px;
`;

export default StyledArtistsContainer;

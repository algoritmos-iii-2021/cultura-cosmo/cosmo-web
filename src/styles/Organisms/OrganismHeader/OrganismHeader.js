import styled from 'vue-styled-components';

const StyledHeader = styled.header`
  background-color: #090538;
  height: 100px;

  .ant-row-flex {
    height: 100%;
  }
`;

export default StyledHeader;

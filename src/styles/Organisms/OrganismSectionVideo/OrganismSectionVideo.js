import styled from 'vue-styled-components';
import imgBackground from '../../../assets/section-video-background.jpg';

const StyledVideoContainer = styled.div`
  background: url(${imgBackground});
  background-size: cover;
  background-position: center;
  background-repeat: no-repeat;
  height: 950px;
`;

export default StyledVideoContainer;

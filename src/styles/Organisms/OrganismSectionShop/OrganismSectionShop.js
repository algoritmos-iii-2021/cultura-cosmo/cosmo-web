import styled from 'vue-styled-components';

const StyledShopContainer = styled.div`
  background: linear-gradient(180deg, #0A0337 0%, #000000 100%);
  padding: 40px 0 60px;

  .VueCarousel {
    width: 100%;
    &-slide {
      display: flex;
      justify-content: center;
    }
    &-navigation-button {
        color: #fff;
        font-size: 50px;
      }
  }
`;

export default StyledShopContainer;

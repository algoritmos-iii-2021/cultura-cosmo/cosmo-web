import styled from 'vue-styled-components';

export const StyledFooterLicense = styled.div`
  padding: 10px;

  p {
      color: white;
    }
`;

export const StyledFooterContainer = styled.div`
  background: #600265;
  padding-top: 20px;

  p {
    color: white;
  }
`;

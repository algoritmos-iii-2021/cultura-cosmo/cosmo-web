import styled from 'vue-styled-components';

const StyledBioContainer = styled.div`
  background: linear-gradient(180deg, #0A0337 10.42%, #600265 50.52%, #C9009D 100%);
  padding: 40px 0 60px;
`;

export default StyledBioContainer;

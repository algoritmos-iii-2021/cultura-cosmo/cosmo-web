import styled from 'vue-styled-components';

const StyledContactForm = styled.div`
  width: 1000px;

  label {
    color: #C9009D;
    font-size: 20px;
  }

  input, textarea {
    background-color: transparent;
    border-color: #C9009D;
    color: white;
  }

  button {
    width: 100%
  }
`;

export default StyledContactForm;

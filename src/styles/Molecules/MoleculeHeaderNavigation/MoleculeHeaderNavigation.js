import styled from 'vue-styled-components';

const StyledUl = styled.ul`
  margin: 0;
  padding: 0;
  display: flex;
  align-items: center;
`;

export default StyledUl;

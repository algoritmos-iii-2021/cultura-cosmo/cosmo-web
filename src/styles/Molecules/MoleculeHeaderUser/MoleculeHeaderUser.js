import styled from 'vue-styled-components';

const StyledDiv = styled.div`
  float: right;

  button {
    margin-left: 20px;
  }
`;

export default StyledDiv;

import styled from 'vue-styled-components';

const StyledNewsletterForm = styled.div`
  width: 1000px;

  label {
    color: #C9009D;
    font-size: 20px;
  }

  input, .ant-select-selection {
    background-color: transparent;
    border-color: #C9009D;
    color: white;
  }

  button {
    width: 100%
  }
`;

export default StyledNewsletterForm;

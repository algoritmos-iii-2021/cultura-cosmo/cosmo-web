import styled from 'vue-styled-components';

export const StyledWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;

  &:hover {
    cursor: pointer;

      > div {
      border: 10px solid white;
    }
      > h3 {
        color: lightgray;
      }
  }
`;

export const StyledCircle = styled.div`
  width: 270px;
  height: 270px;
  border-radius: 50%;
  border: 10px solid #C9009D;
  overflow: hidden;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const StyledImage = styled.img`
  height: 270px;
  width: auto;
`;

export const StyledName = styled.h3`
  color: white;
  margin-top: 20px;
  font-size: 24px;
`;

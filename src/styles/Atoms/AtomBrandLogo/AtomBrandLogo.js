import styled from 'vue-styled-components';

export const StyledDiv = styled.div`
  display: flex;
`;

export const StyledImg = styled.img`
  height: 80px;
  width: auto;
`;

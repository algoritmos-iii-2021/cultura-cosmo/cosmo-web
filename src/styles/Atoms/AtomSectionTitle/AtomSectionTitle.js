import styled from 'vue-styled-components';

const StyledH2 = styled.h2`
  color: white;
  font-size: 48px;
  font-weight: bold;
`;

export default StyledH2;

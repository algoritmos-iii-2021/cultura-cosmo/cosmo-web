import styled from 'vue-styled-components';

const StyledLi = styled.li`
  padding: 0;
  list-style: none;
  color: white;
  font-family: 'Lato', Arial, Helvetica, sans-serif;
  font-weight: bold;
  font-size: 24px;
  padding: 0 30px;
`;

export default StyledLi;

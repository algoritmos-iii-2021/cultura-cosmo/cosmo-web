import styled from 'vue-styled-components';

export const StyledWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  background: linear-gradient(180deg, #600265 0%, #C9009D 100%);
  border-radius: 27px;
  width: 270px;
  height: 480px;
  padding: 20px;

  &:hover {
    cursor: pointer;
    background: linear-gradient(180deg, #C9009D 100%, #600265 0%)
  }

  .coin {
    width: 50px;
    height: auto;
    margin-right: 15px;
  }
`;

export const StyledImage = styled.img`
  height: auto;
  width: 100%;
`;

export const StyledName = styled.h3`
  color: white;
  margin-top: 20px;
  font-size: 24px;
`;

export const StyledLine = styled.hr`
  width: 100%;
  height: 5px;
  background-color: white;
  margin: 0;
`;

export const StyledPrice = styled.p`
  color: white;
  font-size: 36px;
  display: inline;
  margin: 0;
`;

export const StyledPriceWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
`;

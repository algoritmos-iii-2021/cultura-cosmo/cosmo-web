import styled from 'vue-styled-components';

const spaceProps = { space: String };
const flexProps = { direction: String, align: String, justify: String };

export const StyledContainer = styled.div`
    width: 100%;
    height: 100%;
    margin: auto;

    @media (max-width: 576px) { max-width: none }
    @media (min-width: 576px) { max-width: 540px }
    @media (min-width: 768px) { max-width: 720px }
    @media (min-width: 992px) { max-width: 960px }
    @media (min-width: 1200px) { max-width: 1140px }
    @media (min-width: 1400px) { max-width: 1320px }
`;

export const StyledContainerFluid = styled.div`
    width: 100%;
    display: flex;
    align-self: center;
    margin: auto;
`;

export const StyledFlexContainer = styled('div', flexProps)`
    display: flex;
    flex-direction: ${(props) => (props.direction ? props.direction : 'row')};
    align-items: ${(props) => (props.align ? props.align : 'start')};
    justify-content: ${(props) => (props.justify ? props.justify : 'start')};
    width: 100%
`;

export const StyledCardBackground = styled.div`
  background: rgba(0, 0, 0, 0.3);
  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
  border-radius: 30px;
  padding: 60px;
`;

export const StyledParagraph = styled.p`
  font-size: 24px;
  text-indent: 48px;
  color: white;
`;

export const StyledSpace = styled('div', spaceProps)`
    padding: ${(props) => (props.space ? props.space : '10px')} 0 0 0;
`;

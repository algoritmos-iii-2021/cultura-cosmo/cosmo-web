import OrganismHeader from './OrganismHeader.vue';

export default {
  title: 'Organisms/Header',
  component: OrganismHeader,
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { OrganismHeader },
  template:
    '<OrganismHeader :user="user" @onLogin="onLogin" @onLogout="onLogout" @onCreateAccount="onCreateAccount" />',
});

export const LoggedIn = Template.bind({});
LoggedIn.args = {
  user: {},
};

export const LoggedOut = Template.bind({});
LoggedOut.args = {};

import AtomBrandLogo from './AtomBrandLogo.vue';

export default {
  title: 'Atoms/Button',
  component: AtomBrandLogo,
  argTypes: {
    size: { control: { type: 'select', options: ['small', 'medium', 'large'] } },
    type: { control: { type: 'select', options: ['primary', 'outlined', 'dashed', 'danger', 'danger', 'link'] } },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { AtomBrandLogo },
  template: '<AtomBrandLogo />',
});

export const Primary = Template.bind({});
Primary.args = {
  type: 'primary',
  label: 'Button',
};

export const Default = Template.bind({});
Default.args = {
  type: 'outlined',
  label: 'Button',
};

export const Large = Template.bind({});
Large.args = {
  size: 'large',
  label: 'Button',
};

export const Small = Template.bind({});
Small.args = {
  size: 'small',
  label: 'Button',
};
